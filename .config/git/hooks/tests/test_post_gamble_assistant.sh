#!/usr/bin/env sh

REAL_PATH=$(realpath "$0")
DIRECTORY_PATH=$(dirname "${REAL_PATH}")
HOOKS_PATH=$(dirname "${DIRECTORY_PATH}")

test_you_can_push_when_gambled_that_test_pass_and_actually_pass() {
    OUTPUT="$(${HOOKS_PATH}/post-gamble.assistant.sample.sh pass pass)"

    assertEquals "You can push" "${OUTPUT}"
}

test_do_not_push_when_gambled_that_test_fail_and_actually_fail() {
    OUTPUT="$(${HOOKS_PATH}/post-gamble.assistant.sample.sh fail fail)"

    assertEquals "Do not push" "${OUTPUT}"
}

test_the_new_test_should_verify_a_new_behavior_when_gambled_that_test_fail_but_actually_pass() {
    OUTPUT="$(${HOOKS_PATH}/post-gamble.assistant.sample.sh fail pass)"

    assertEquals "The new test should verify a new behavior" "${OUTPUT}"
}

test_maybe_too_hard_step_when_gambled_that_test_pass_but_actually_fail() {
    OUTPUT="$(${HOOKS_PATH}/post-gamble.assistant.sample.sh pass fail)"

    assertEquals "Maybe try a smaller step, if your test is too hard you can delete it (git reset --hard HEAD~1) and then refactor to help yourself with your next step https://twitter.com/KentBeck/status/250733358307500032" "${OUTPUT}"
}

. shunit2
