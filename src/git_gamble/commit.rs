use std::io;

use yansi::Paint;

use super::cli::Cli;
use super::repository::Repository;

pub fn commit(opt: &Cli, repository: &Repository) -> Result<i32, io::Error> {
	log::info!("commit");

	let post_hook_gambled = if opt.pass { "pass" } else { "fail" };
	let post_hook_actual = post_hook_gambled;

	if repository.is_clean() {
		repository.run_hook("post-gamble", &[post_hook_gambled, post_hook_actual])?;

		log::warn!("There is nothing to commit");
		println!("{}", "There is nothing to commit".yellow().bold());

		return Ok(1);
	} else {
		let base_options = vec!["commit", "--allow-empty-message"];

		let mut message_options = if let Some(commit) = &opt.fixup {
			vec!["--fixup", commit]
		} else if opt.message.is_empty() && repository.head_is_failing_ref() {
			vec!["--reuse-message", "HEAD"]
		} else {
			vec![]
		};

		if !opt.message.is_empty() {
			message_options.extend_from_slice(&["--message", &opt.message]);
		}

		let edit_options = if opt.edit {
			vec!["--edit"]
		} else {
			vec!["--no-edit"]
		};

		let squash_options = if let Some(commit) = &opt.squash {
			vec!["--squash", commit]
		} else {
			vec![]
		};

		let amend_options = if repository.head_is_failing_ref() {
			vec!["--amend"]
		} else {
			vec![]
		};

		let no_verify_options = if opt.no_verify {
			vec!["--no-verify"]
		} else {
			vec![]
		};

		repository.command(
			&[
				base_options,
				message_options,
				edit_options,
				squash_options,
				amend_options,
				no_verify_options,
			]
			.concat(),
		)?;

		if opt.fail {
			repository.command(&["update-ref", "refs/gamble-is-failing", "HEAD"])?;
		}

		repository.run_hook("post-gamble", &[post_hook_gambled, post_hook_actual])?;

		log::info!("committed");
		println!("{}", "Committed!".blue().bold());
	};

	Ok(0)
}
