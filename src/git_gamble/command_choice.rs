use std::io;
use std::process::ExitStatus;

use super::cli::Cli;

use super::repository::Repository;

use super::commit::commit;
use super::revert::revert;

type RepositoryCommand = fn(opt: &Cli, repository: &Repository) -> Result<i32, io::Error>;

pub fn tcr(status: ExitStatus) -> RepositoryCommand {
	if status.success() {
		commit
	} else {
		revert
	}
}

pub fn trc(status: ExitStatus) -> RepositoryCommand {
	if status.success() {
		revert
	} else {
		commit
	}
}
