use std::io;

use yansi::Paint;

use super::cli::Cli;
use super::repository::Repository;

pub fn revert(opt: &Cli, repository: &Repository) -> Result<i32, io::Error> {
	log::info!("revert");

	repository.command(&["reset", "--hard"])?;

	repository.run_hook(
		"post-gamble",
		&[
			if opt.pass { "pass" } else { "fail" },
			if opt.pass { "fail" } else { "pass" },
		],
	)?;

	log::info!("reverted");
	println!("{}", "Reverted!".red().bold());

	Ok(0)
}
