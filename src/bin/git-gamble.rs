use clap::Parser;
use clap_complete::generate;
use clap_complete::Shell;
use std::io;
use std::process::exit;
use std::process::Command;

use git_gamble::git_gamble::cli::Cli;
use git_gamble::git_gamble::cli::OptionalSubcommands;

use git_gamble::git_gamble::repository::Repository;

use git_gamble::git_gamble::command_choice::tcr;
use git_gamble::git_gamble::command_choice::trc;

use git_gamble::git_gamble::generate_shell_completions::generate_shell_completions;

fn main() {
	#[cfg(feature = "with_log")]
	pretty_env_logger::init();

	human_panic::setup_panic!();

	let opt = Cli::parse();
	log::info!("{:#?}", opt);

	if let Some(subcommand_opt) = opt.optional_subcommands {
		match subcommand_opt {
			OptionalSubcommands::GenerateShellCompletions(generate_shell_completions_opt) => {
				generate_shell_completions(env!("CARGO_BIN_NAME"), |package_name, command| {
					let shell = generate_shell_completions_opt
						.shell
						.or_else(Shell::from_env)
						.unwrap_or(Shell::Bash);
					generate(shell, command, package_name, &mut io::stdout())
				})
			}
		}
		exit(0)
	}

	exit(gamble(opt).unwrap())
}

fn gamble(opt: Cli) -> io::Result<i32> {
	let gamble_strategy = if opt.pass { tcr } else { trc };

	let maybe_test_command = reformat_command(opt.test_command.clone());

	if maybe_test_command.is_none() {
		log::error!("Can't parse test command");
		return Ok(1);
	}

	let repository = Repository::new(opt.repository_path.as_path(), opt.dry_run, opt.no_verify);

	let hook_gambled = if opt.pass { "pass" } else { "fail" };
	repository.run_hook("pre-gamble", &[hook_gambled])?;

	repository.command(&["add", "--all"])?;

	let test_command = maybe_test_command.unwrap();
	let status = Command::new(&test_command[0])
		.args(&test_command[1..])
		.status()
		.expect("failed to run tests");
	let command_choice = gamble_strategy(status);

	let result = command_choice(&opt, &repository)?;

	Ok(result)
}

fn reformat_command(command: Vec<String>) -> Option<Vec<String>> {
	if command.len() == 1 {
		shlex::split(command[0].as_str())
	} else {
		Some(command)
	}
}
