stages:
    - build-container
    - check
    - build-release
    - package
    - publish
    - release

image: rust:1.80-slim-bookworm

variables:
    APT_CACHE_DIR: ".apt/"
    CARGO_INSTALL_ROOT: ".cargo_install_root/"
    CARGO_HOME: ".cargo/"
    LINUX_DEPLOY_IMAGE_NAME: $CI_REGISTRY_IMAGE/linuxdeploy
    RUST_FOR_WINDOWS_IMAGE_NAME: $CI_REGISTRY_IMAGE/rust-for-windows
    CARGO_AUDIT_IMAGE_NAME: $CI_REGISTRY_IMAGE/cargo-audit
    CARGO_DEB_IMAGE_NAME: $CI_REGISTRY_IMAGE/cargo-deb
    SHUNIT2_IMAGE_NAME: $CI_REGISTRY_IMAGE/shunit2
    CARGO_TARPAULIN_IMAGE_NAME: $CI_REGISTRY_IMAGE/cargo-tarpaulin
    MDBOOK_IMAGE_NAME: $CI_REGISTRY_IMAGE/md-book
    PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"
    CACHIX_CACHE_NAME: git-gamble

.cache_paths_rust: &cache_paths_rust
    key:
        files:
            - flake.lock
            - Cargo.lock
    paths:
        - $APT_CACHE_DIR
        - $CARGO_INSTALL_ROOT
        - $CARGO_HOME
        - target/

.cache_paths_javascript: &cache_paths_javascript
    key:
        files:
            - flake.lock
            - slides/pnpm-lock.yaml
    paths:
        - slides/.pnpm-store/

.build-container-with-kaniko: &build-container-with-kaniko
    image:
        name: gcr.io/kaniko-project/executor:v1.19.2-debug
        entrypoint: [""]

build container for LinuxDeploy:
    stage: build-container
    rules:
        - changes:
              - "packaging/AppImage/"
              - ".gitlab-ci.yml"
              - "rust-toolchain.toml"
    needs: []
    <<: *build-container-with-kaniko
    script:
        - /kaniko/executor
          --context $CI_PROJECT_DIR/packaging/AppImage/
          --dockerfile $CI_PROJECT_DIR/packaging/AppImage/Dockerfile
          --cache
          --destination $LINUX_DEPLOY_IMAGE_NAME

build container for Rust for Windows:
    stage: build-container
    rules:
        - changes:
              - "ci/rust_for_windows/"
              - ".gitlab-ci.yml"
              - "rust-toolchain.toml"
    needs: []
    <<: *build-container-with-kaniko
    script:
        - /kaniko/executor
          --context $CI_PROJECT_DIR/ci/rust_for_windows/
          --dockerfile $CI_PROJECT_DIR/ci/rust_for_windows/Dockerfile
          --cache
          --destination $RUST_FOR_WINDOWS_IMAGE_NAME

build container for cargo deb:
    stage: build-container
    rules:
        - changes:
              - "packaging/debian/Dockerfile"
              - ".gitlab-ci.yml"
              - "rust-toolchain.toml"
    needs: []
    <<: *build-container-with-kaniko
    script:
        - /kaniko/executor
          --dockerfile $CI_PROJECT_DIR/packaging/debian/Dockerfile
          --cache
          --destination $CARGO_DEB_IMAGE_NAME

build container for shunit2:
    stage: build-container
    rules:
        - changes:
              - "ci/shunit2.Dockerfile"
              - ".gitlab-ci.yml"
    needs: []
    <<: *build-container-with-kaniko
    script:
        - /kaniko/executor
          --dockerfile $CI_PROJECT_DIR/ci/shunit2.Dockerfile
          --cache
          --destination $SHUNIT2_IMAGE_NAME

build container for cargo audit:
    stage: build-container
    rules:
        - changes:
              - "ci/cargo-audit.Dockerfile"
              - ".gitlab-ci.yml"
              - "rust-toolchain.toml"
    needs: []
    <<: *build-container-with-kaniko
    script:
        - /kaniko/executor
          --dockerfile $CI_PROJECT_DIR/ci/cargo-audit.Dockerfile
          --cache
          --destination $CARGO_AUDIT_IMAGE_NAME

build container for cargo tarpaulin:
    stage: build-container
    rules:
        - changes:
              - "ci/cargo-tarpaulin.Dockerfile"
              - ".gitlab-ci.yml"
              - "rust-toolchain.toml"
    needs: []
    <<: *build-container-with-kaniko
    script:
        - /kaniko/executor
          --dockerfile $CI_PROJECT_DIR/ci/cargo-tarpaulin.Dockerfile
          --cache
          --destination $CARGO_TARPAULIN_IMAGE_NAME

build container for mdBook:
    stage: build-container
    rules:
        - changes:
              - "ci/mdBook.Dockerfile"
              - ".gitlab-ci.yml"
    needs: []
    <<: *build-container-with-kaniko
    script:
        - /kaniko/executor
          --dockerfile $CI_PROJECT_DIR/ci/mdBook.Dockerfile
          --cache
          --destination $MDBOOK_IMAGE_NAME

cargo audit:
    stage: check
    image: $CARGO_AUDIT_IMAGE_NAME
    cache:
        <<: *cache_paths_rust
    script:
        - cargo audit

cargo check:
    stage: check
    needs: []
    script:
        - cargo check
          --all-targets
          --all-features

check format:
    stage: check
    needs: []
    before_script:
        - rustup component add rustfmt
    script:
        - cargo fmt --
          --check

clippy:
    stage: check
    needs: []
    before_script:
        - rustup component add clippy
    script:
        - cargo clippy
          --all-targets
          --all-features

test shell scripts:
    stage: check
    image: $SHUNIT2_IMAGE_NAME
    script:
        - ./tests/test_scripts.sh

.install_git: &install_git # git is needed by the tests to be in $PATH
    - apt-get update &&
      apt-get install -y --no-install-recommends --option dir::cache::archives="$APT_CACHE_DIR"
      git

test rust:
    stage: check
    needs: []
    before_script: *install_git
    script:
        - cargo test

test rust with coverage:
    stage: check
    image: $CARGO_TARPAULIN_IMAGE_NAME
    before_script: *install_git
    script:
        - cargo tarpaulin
          --locked
          --out Xml
    retry:
        max: 2
        when: script_failure
    coverage: '/\d+\.\d+% coverage/'
    artifacts:
        expire_in: 1 week
        reports:
            coverage_report:
                coverage_format: cobertura
                path: cobertura.xml

build release for Linux:
    stage: build-release
    needs:
        - test rust
    cache:
        <<: *cache_paths_rust
    script:
        - cargo build
          --locked
          --release
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-build-release-for-linux"
        expire_in: 1 week
        paths:
            - target/release/git-gamble

build release for Windows:
    stage: build-release
    needs:
        - test rust
    image: $RUST_FOR_WINDOWS_IMAGE_NAME
    cache:
        <<: *cache_paths_rust
        key:
            prefix: windows
            files:
                - flake.lock
                - Cargo.lock
    script:
        - mkdir -p ${CARGO_HOME}
        - cp /usr/local/cargo/config ${CARGO_HOME}/config
        - rustup target add x86_64-pc-windows-gnu
        - cargo build
          --locked
          --release
          --target x86_64-pc-windows-gnu
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-build-release-for-windows"
        expire_in: 1 week
        paths:
            - target/x86_64-pc-windows-gnu/release/git-gamble.exe

.packages_variables: &packages_variables
    - export VERSION=${CI_COMMIT_TAG##*/}

    - export LINUX_FILE="git-gamble_v${VERSION}_$(uname -m)_linux"
    - export LINUX_URL="${PACKAGE_REGISTRY_URL}/git-gamble-linux/${VERSION}/${LINUX_FILE}"

    - export WINDOWS_FILE="git-gamble_v${VERSION}_$(uname -m)_windows.exe"
    - export WINDOWS_URL="${PACKAGE_REGISTRY_URL}/git-gamble-windows/${VERSION}/${WINDOWS_FILE}"

    - export APPIMAGE_FILE="git-gamble-v${VERSION}-$(uname -m).AppImage"
    - export APPIMAGE_URL="${PACKAGE_REGISTRY_URL}/git-gamble-AppImage/${VERSION}/${APPIMAGE_FILE}"

    - export DEB_FILE="git-gamble_${VERSION}_$(uname -m).deb"
    - export DEB_URL="${PACKAGE_REGISTRY_URL}/git-gamble-debian/${VERSION}/${DEB_FILE}"

    - export CHOCOLATEY_FILE="git-gamble.portable.${VERSION}.nupkg"
    - export CHOCOLATEY_URL="${PACKAGE_REGISTRY_URL}/git-gamble.portable/${VERSION}/${CHOCOLATEY_FILE}"

package Deb:
    stage: package
    needs:
        - build release for Linux
    image: $CARGO_DEB_IMAGE_NAME
    cache:
        <<: *cache_paths_rust
    script:
        - export PATH="$PATH:target/release/"
        - ./script/generate_completion.sh target/release/shell_completions/
        - cargo deb
          --
          --locked
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-debian"
        expire_in: 1 week
        paths:
            - target/debian/

package AppImage:
    stage: package
    needs:
        - build release for Linux
    image:
        name: $LINUX_DEPLOY_IMAGE_NAME
        entrypoint: [""]
    before_script: *packages_variables
    script:
        - mkdir -p target/AppDir/usr/share/doc/git-gamble/
        - ./script/usage.sh >target/AppDir/usr/share/doc/git-gamble/README
        - linuxdeploy
          --appdir=target/AppDir
          --executable=target/release/git-gamble
          --desktop-file=packaging/AppImage/AppDir/git-gamble.desktop
          --icon-file=docs/src/assets/logo/git-gamble.svg
          --output appimage
        - mv *.AppImage target/${APPIMAGE_FILE}
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-appimage"
        expire_in: 1 week
        paths:
            - "target/${APPIMAGE_FILE}"

package Chocolatey:
    stage: package
    needs:
        - build release for Windows
    image:
        name: thinkco/chocolatey:0.10.14
        entrypoint: [""]
    script:
        - mv target/x86_64-pc-windows-gnu/release/git-gamble.exe packaging/Chocolatey/tools/
        - cd packaging/Chocolatey/
        - export CHECKSUM=$(
          shasum
          --algorithm 256
          tools/git-gamble.exe
          |
          cut
          --delimiter " "
          --fields 1
          )
        - sed
          --in-place
          "s/CHECKSUM/${CHECKSUM}/"
          tools/chocolateyinstall.ps1
        - export CHOCOLATEY_VERSION=$(
          echo ${CI_COMMIT_TAG}
          |
          sed
          --regexp-extended
          "s%.*([0-9]+\.[0-9]+\.[0-9]+(-\w+)?).*%\1%"
          )
        - mono /opt/chocolatey/choco.exe pack
          --allow-unofficial
          --version=${CHOCOLATEY_VERSION}
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-chocolatey"
        expire_in: 1 week
        paths:
            - "packaging/Chocolatey/*.nupkg"

.if-is-releasing: &if-is-releasing
    if: '$CI_COMMIT_REF_NAME =~ /^version\/.+$/'

.if-is-releasing-stable-version: &if-is-releasing-stable-version
    if: '$CI_COMMIT_TAG =~ /^version\/\d+\.\d+\.\d+$/'

.nix_with_flake: &nix_with_flake
    - mkdir --parents ~/.config/nix/
    - echo "experimental-features = nix-command flakes" >~/.config/nix/nix.conf

.nix_with_cachix: &nix_with_cachix
    - nix-env --install --attr nixpkgs.cachix
    - cachix use "$CACHIX_CACHE_NAME"

push to cachix:
    stage: publish
    rules:
        - if: '$CI_COMMIT_REF_PROTECTED == "true" && "$CACHIX_CACHE_NAME" != ""'
    needs: []
    image: nixos/nix:2.16.1
    before_script:
        - *nix_with_flake
        - *nix_with_cachix
    script:
        - cachix watch-exec $CACHIX_CACHE_NAME
          nix run
          ".#git-gamble"
          --
          --
          --help

build website:
    stage: package
    image: $MDBOOK_IMAGE_NAME
    cache:
        <<: *cache_paths_rust
    variables:
        CARGO_HOME: $CI_PROJECT_DIR/cargo
    script:
        - mdbook build ./docs/ --dest-dir ../public/
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-website"
        expire_in: 1 week
        paths:
            - public/

.pnpm: &pnpm
    - corepack enable
    - corepack prepare pnpm --activate
    - pnpm config set store-dir .pnpm-store

build slides:
    stage: package
    image: node:21-alpine
    cache:
        <<: *cache_paths_javascript
    before_script:
        - cd ./slides
        - *pnpm
        - pnpm install
    script:
        - pnpm run build --base /slides_theory/ --out ./dist/slides_theory ./slides_theory.md
        - pnpm run build --base /slides_demo/ --out ./dist/slides_demo ./slides_demo.md
        - pnpm run build --base /slides_why/ --out ./dist/slides_why ./slides_why.md
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-website"
        expire_in: 1 week
        paths:
            - slides/dist/

pages:
    stage: publish
    rules:
        - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    image: node:21-alpine
    cache:
        <<: *cache_paths_javascript
    before_script:
        - apk add brotli
        - cd ./slides
        - *pnpm
        - pnpm install --prod
        - cd ..
    script:
        - cat ./slides/dist/*/_redirects >>./public/_redirects
        - mv ./slides/dist/* ./public/
        # https://github.com/rust-lang/mdBook/issues/1491#issuecomment-1037671733
        - cd ./slides
        - pnpm run sitemap
          --base "https://git-gamble.is-cool.dev"
          --root "../public"
          --format "xml"
          --no-clean
          --ignore "**/404.html"
          --ignore "**/_*"
          --ignore "**/wip/**"
          --ignore "**/backlog*"
          --ignore "**/google*.html"
          --ignore "**/BingSiteAuth.xml"
          --ignore "**/yandex*.html"
        - cd ..
        - find ./public
          -type f
          -exec brotli --quality=11 {} \;
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-pages"
        expire_in: 1 week
        paths:
            - public/

publish crates:
    stage: publish
    rules:
        - <<: *if-is-releasing
    script:
        - cargo publish
          --locked
          --token $CRATES_IO_TOKEN
    artifacts:
        name: "$CI_COMMIT_REF_SLUG-publish"
        expire_in: 1 week
        paths:
            - target/package/

publish on GitLab:
    stage: publish
    needs:
        - build release for Linux
        - build release for Windows
        - package AppImage
        - package Deb
    rules:
        - <<: *if-is-releasing-stable-version
    image: curlimages/curl:7.83.0
    before_script: *packages_variables
    script:
        - >
            curl
            --fail
            --header "JOB-TOKEN: $CI_JOB_TOKEN"
            --upload-file "target/release/git-gamble"
            "${LINUX_URL}"
        - >
            curl
            --fail
            --header "JOB-TOKEN: $CI_JOB_TOKEN"
            --upload-file "target/x86_64-pc-windows-gnu/release/git-gamble.exe"
            "${WINDOWS_URL}"
        - >
            curl
            --fail
            --header "JOB-TOKEN: $CI_JOB_TOKEN"
            --upload-file target/*.AppImage
            "${APPIMAGE_URL}"
        - >
            curl
            --fail
            --header "JOB-TOKEN: $CI_JOB_TOKEN"
            --upload-file target/debian/*.deb
            "${DEB_URL}"

publish Chocolatey on GitLab:
    stage: publish
    needs:
        - package Chocolatey
    rules:
        - <<: *if-is-releasing
    image: chocolatey/choco:v2.3.0
    script:
        - choco source add
          --source "'${CI_SERVER_URL}/api/v4/projects/${CI_PROJECT_ID}/packages/nuget/v2'"
          --name=gitlab
          --user gitlab-ci-token
          --password ${CI_JOB_TOKEN}
        - choco push
          "packaging/Chocolatey/*.nupkg"
          --source gitlab

release on GitLab:
    stage: release
    image: registry.gitlab.com/gitlab-org/release-cli:v0.11.0
    rules:
        - <<: *if-is-releasing-stable-version
    before_script: *packages_variables
    script:
        - export DESCRIPTION=$(./ci/display_changelog_of_version.sh "${VERSION}")
        - release-cli create
          --tag-name "${CI_COMMIT_TAG}"
          --description "${DESCRIPTION}"
          --assets-link '{"name":"'${LINUX_FILE}'","url":"'${LINUX_URL}'","link_type":"package"}'
          --assets-link '{"name":"'${WINDOWS_FILE}'","url":"'${WINDOWS_URL}'","link_type":"package"}'
          --assets-link '{"name":"'${APPIMAGE_FILE}'","url":"'${APPIMAGE_URL}'","link_type":"package"}'
          --assets-link '{"name":"'${DEB_FILE}'","url":"'${DEB_URL}'","link_type":"package"}'
          --assets-link '{"name":"'${CHOCOLATEY_FILE}'","url":"'${CHOCOLATEY_URL}'","link_type":"package"}'
