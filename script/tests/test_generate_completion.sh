#!/usr/bin/env sh

set -o errexit
set -o nounset

REAL_PATH=$(realpath "${0}")

DIRECTORY_PATH=$(dirname "$(dirname "${REAL_PATH}")")

. "$(dirname $REAL_PATH)/test_file_assertion.sh"

test_generate_completion_for_bash() {
    "${DIRECTORY_PATH}/generate_completion.sh" "$SHUNIT_TMPDIR"

    assert_file_exists "$SHUNIT_TMPDIR/git-gamble.bash"
}

test_generate_completion_for_elvish() {
    "${DIRECTORY_PATH}/generate_completion.sh" "$SHUNIT_TMPDIR"

    assert_file_exists "$SHUNIT_TMPDIR/git-gamble.elv"
}

test_generate_completion_for_fish() {
    "${DIRECTORY_PATH}/generate_completion.sh" "$SHUNIT_TMPDIR"

    assert_file_exists "$SHUNIT_TMPDIR/git-gamble.fish"
}

test_generate_completion_for_powershell() {
    "${DIRECTORY_PATH}/generate_completion.sh" "$SHUNIT_TMPDIR"

    assert_file_exists "$SHUNIT_TMPDIR/_git-gamble.ps1"
}

test_generate_completion_for_zsh() {
    "${DIRECTORY_PATH}/generate_completion.sh" "$SHUNIT_TMPDIR"

    assert_file_exists "$SHUNIT_TMPDIR/_git-gamble"
}

. shunit2
