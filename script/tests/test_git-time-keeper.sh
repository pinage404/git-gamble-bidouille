#!/usr/bin/env sh

set -o errexit
set -o nounset

REAL_PATH=$(realpath "${0}")
DIRECTORY_PATH=$(dirname "$(dirname "${REAL_PATH}")")

INITIAL_WORKING_DIRECTORY=$(pwd)

TIME_KEEPER_FILE=""

CUSTOM_TMPDIR=""

. "$(dirname $REAL_PATH)/test_file_assertion.sh"

oneTimeSetUp() {
	cd "${SHUNIT_TMPDIR}"
	git init --quiet
	git config user.name "Time Keeper"
	git config user.email "time.keeper@git.gamble"
	echo "Something committed" >foo
	git add foo
	git commit --message "Initial commit" --quiet
	cd "${INITIAL_WORKING_DIRECTORY}"
}

setUp() {
	TIME_KEEPER_FILE="/tmp/time_keeper_$(basename ${SHUNIT_TMPDIR})"

	export TIME_KEEPER_MAXIMUM_ITERATION_DURATION=0.1

	cd "${SHUNIT_TMPDIR}"
	echo "Something dirty" >foo

	CUSTOM_TMPDIR=$(mktemp --directory)
}

tearDown() {
	sleep 0.1 # ensure that previous run is finished

	git restore .

	rm --force --recursive "${TIME_KEEPER_FILE}" "${CUSTOM_TMPDIR}"
}

test_do_nothing_when_starting_and_stopping() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null
	"${DIRECTORY_PATH}/git-time-keeper" "stop"

	sleep 0.1

	assert_repository_is_dirty
}

test_write_file_when_starting() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	assert_file_exists "${TIME_KEEPER_FILE}"
}

test_respect_TMPDIR() {
	TMPDIR="${CUSTOM_TMPDIR}" "${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	TIME_KEEPER_FILE="${CUSTOM_TMPDIR}/time_keeper_$(basename ${SHUNIT_TMPDIR})"
	assert_file_exists "${TIME_KEEPER_FILE}"
}

test_do_not_write_file_when_no_duration() {
	unset TIME_KEEPER_MAXIMUM_ITERATION_DURATION

	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	assert_file_does_not_exist "${TIME_KEEPER_FILE}"
}

test_remove_file_when_stopping() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null
	"${DIRECTORY_PATH}/git-time-keeper" "stop"

	assert_file_does_not_exist "${TIME_KEEPER_FILE}"
}

test_do_not_fail_when_stopping_without_starting() {
	"${DIRECTORY_PATH}/git-time-keeper" "stop"

	assert_file_does_not_exist "${TIME_KEEPER_FILE}"
}

test_do_nothing_before_the_delay_when_starting() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	assert_repository_is_dirty
}

test_remove_changes_when_starting_and_waiting() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	sleep 0.1

	sleep 0.1 # prevent flaky test

	assert_repository_is_clean
}

test_do_nothing_when_starting_and_file_does_not_exists() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	rm "${TIME_KEEPER_FILE}"

	sleep 0.1

	assert_repository_is_dirty
}

test_remove_file_when_starting_and_waiting() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	sleep 0.1

	sleep 0.1 # prevent flaky test

	assert_file_does_not_exist "${TIME_KEEPER_FILE}"
}

test_display_reverted_when_starting_and_waiting() {
	OUTPUT=$(
		"${DIRECTORY_PATH}/git-time-keeper" "start"

		sleep 0.1

		sleep 0.1 # prevent flaky test
	)

	assertContains "${OUTPUT}" "Reverted!"
}

test_display_time_out_when_starting_and_waiting() {
	OUTPUT=$(
		"${DIRECTORY_PATH}/git-time-keeper" "start"

		sleep 0.1

		sleep 0.1 # prevent flaky test
	)

	assertContains "${OUTPUT}" "Timed out!"
}

test_run_given_command_when_starting_with_a_custom_command_and_waiting() {
	OUTPUT=$(
		"${DIRECTORY_PATH}/git-time-keeper" "start" "echo custom_command_output"

		sleep 0.1

		sleep 0.1 # prevent flaky test
	)

	assertContains "${OUTPUT}" "custom_command_output"
}

test_do_not_display_reverted_when_starting_with_a_custom_command_and_waiting() {
	OUTPUT=$(
		"${DIRECTORY_PATH}/git-time-keeper" "start" "echo custom_command_output"

		sleep 0.1

		sleep 0.1 # prevent flaky test
	)

	assertNotContains "${OUTPUT}" "Reverted!"
}

test_exit_when_starting_twice() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null && fail "should not start twice" || true
}

test_kill_previous_execution_when_starting_then_stopping() {
	"${DIRECTORY_PATH}/git-time-keeper" "start" >/dev/null

	"${DIRECTORY_PATH}/git-time-keeper" "stop" >/dev/null

	"${DIRECTORY_PATH}/git-time-keeper" "start" "true" >/dev/null

	sleep 0.1

	sleep 0.1 # prevent flaky test

	assert_repository_is_dirty
}

assert_repository_is_dirty() {
	assertEquals "Something dirty" "$(cat foo)"
}

assert_repository_is_clean() {
	assertEquals "Something committed" "$(cat foo)"
}

. shunit2
