#!/usr/bin/env bash

# fail on first error
set -o errexit
set -o nounset
set -o pipefail

PROJECT_ROOT=$(realpath "$(dirname "$(realpath "$0")")/..")

source "${PROJECT_ROOT}/script/update_usage.sh"

source "${PROJECT_ROOT}/script/update_master_branch.sh"
