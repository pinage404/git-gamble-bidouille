#!/usr/bin/env bash

# fail on first error
set -o errexit
set -o nounset
set -o pipefail

# master branch is needed for homebrew
git switch master
git rebase -
git push
git switch -
