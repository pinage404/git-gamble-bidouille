{ lib
, stdenv
, inputs
, nix
, rustPlatform
, darwin
, CoreServices ? darwin.apple_sdk.frameworks.CoreServices
, installShellFiles
,
}:

rustPlatform.buildRustPackage {
  pname = "mdbook";
  version = "versionned-by-nix-flake";

  src = inputs.mdbook;

  cargoHash = "sha256-5yI+yTah1K025SCv7a+R0UFO+ouDT2/uIrVvDzGWvH8=";

  nativeBuildInputs = [ installShellFiles ];

  buildInputs = lib.optionals stdenv.isDarwin [ CoreServices ];

  postInstall = lib.optionalString (stdenv.buildPlatform.canExecute stdenv.hostPlatform) ''
    installShellCompletion --cmd mdbook \
      --bash <($out/bin/mdbook completions bash) \
      --fish <($out/bin/mdbook completions fish) \
      --zsh  <($out/bin/mdbook completions zsh )
  '';

  doCheck = false;

  passthru = {
    tests = {
      inherit nix;
    };
  };

  meta = {
    description = "Create books from MarkDown";
    mainProgram = "mdbook";
    homepage = "https://github.com/rust-lang/mdBook";
    changelog = "https://github.com/rust-lang/mdBook/blob/master/CHANGELOG.md";
    license = [ lib.licenses.mpl20 ];
    maintainers = with lib.maintainers; [
      havvy
      Frostman
      matthiasbeyer
    ];
  };
}
