{ pkgs
}:

with pkgs;
[
  mask
  masklint
  shellcheck

  # rust environment
  rustup
  gcc
  gnugrep

  # generate documentation
  mdbook
  mdbook-mermaid
  mdbook-yml-header

  # needed by git-gamble's tests
  bash
  shunit2
  coreutils

  # needed by git-gamble itself
  git

  # needed only when releasing a new version
  cargo-release

  # needed only to check crates vulnerabilities
  cargo-audit

  # needed only to detect what can be improved
  cargo-bloat
  cargo-diet
  cargo-license
  cargo-mutants

  # usefull for refactoring session
  cargo-watch

  # needed by VSCode extension to format Nix files
  nil

  # bash still usable in interactive mode
  bashInteractive
]
++ lib.optionals stdenv.isx86_64 [
  # needed only to know the test coverage
  cargo-tarpaulin
]
++ lib.optionals stdenv.isLinux [
  # needed by `cargo build --release`
  glibc
]
