FROM rust:1.80-slim-bookworm

RUN apt-get --quiet=2 update \
	&& apt-get --quiet=2 install --no-install-recommends \
	libssl-dev \
	pkg-config \
	&& apt-get --quiet=2 clean \
	&& rm --force --recursive /var/lib/apt/lists/*

RUN cargo install cargo-tarpaulin
