FROM rust:1.80-slim-bookworm

RUN export PATH="$PATH:$CARGO_INSTALL_ROOT/bin"
RUN cargo install --git https://github.com/pinage404/mdBook.git mdbook \
	--no-default-features \
	--features search \
	--version "^0.4" \
	--locked
RUN cargo install mdbook-mermaid \
	--version "^0.12" \
	--locked
RUN cargo install mdbook-yml-header \
	--version "^0.1" \
	--locked
