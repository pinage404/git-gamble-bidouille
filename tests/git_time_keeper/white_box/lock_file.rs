use speculoos::assert_that;
use std::env;
use std::fs;
use std::io;
use std::process::Command;
use std::thread;
use std::time::Duration;

use crate::fixture::delay_to_ensure_that_the_file_system_is_synced;
use crate::fixture::CommandIgnoreOutputExt;
use crate::fixture::CommandWithCrateBinariesInThePathExt;
use crate::fixture::TestRepository;
use crate::fixture::TestRepositoryLockFileAssertions;

#[test]
fn create_a_lock_file_when_starting() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();
	assert_that!(&test_repository).count_lock_files(1)
}

#[test]
fn delete_all_lock_files_when_stopping() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();

	let lock_file_folder = test_repository.get_time_keeper_lock_files_folder();
	fs::write(lock_file_folder.join("123.pid"), "")?;
	fs::write(lock_file_folder.join("456.pid"), "")?;
	fs::write(lock_file_folder.join("7890.pid"), "")?;

	delay_to_ensure_that_the_file_system_is_synced();

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["stop"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();
	assert_that!(&test_repository).count_lock_files(0)
}

#[test]
fn the_lock_file_is_in_the_git_dir() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let current_dir = test_repository.path().join("some_subfolder");
	fs::create_dir(&current_dir)?;
	env::set_current_dir(&current_dir)?;

	Command::new("git-time-keeper")
		.current_dir(current_dir)
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();
	assert_that!(&test_repository).count_lock_files(1)
}

#[test]
fn remove_the_lock_file_at_the_end_of_iteration() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "1")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	thread::sleep(Duration::from_secs(1));

	delay_to_ensure_that_the_file_system_is_synced();
	delay_to_ensure_that_the_file_system_is_synced();
	assert_that!(&test_repository).count_lock_files(0)
}

// FIXME: help wanted this test is flaky for unknown reason, disable it while stabilizing it
#[ignore]
#[test]
fn create_as_many_as_lock_files_when_starting_several_times() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "2")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	Command::new("git-time-keeper")
		.current_dir(test_repository.path())
		.with_crate_binaries_in_the_path()
		.env("TIME_KEEPER_MAXIMUM_ITERATION_DURATION", "2")
		.args(["start"])
		.ignore_output()
		.spawn()?;

	delay_to_ensure_that_the_file_system_is_synced();
	delay_to_ensure_that_the_file_system_is_synced();
	delay_to_ensure_that_the_file_system_is_synced();
	delay_to_ensure_that_the_file_system_is_synced();
	delay_to_ensure_that_the_file_system_is_synced();
	assert_that!(&test_repository).count_lock_files(2)
}
