use assert_cmd::Command;
use std::fs::create_dir_all;
use std::fs::read_to_string as read_filecontent;
use std::fs::set_permissions;
use std::fs::write as write_filecontent;
use std::io;
use std::path::Path;
use std::path::PathBuf;
use std::str;
use tempfile::tempdir;
use tempfile::TempDir;

const FIRST_CONTENT: &str = "first content";
const SECOND_CONTENT: &str = "second content";
pub const THIRD_CONTENT: &str = "third content";

pub struct TestRepository {
	temporary_directory: TempDir,
	last_commit_hash_before: Option<String>,
}

impl TestRepository {
	fn new() -> io::Result<Self> {
		Ok(Self {
			temporary_directory: tempdir()?,
			last_commit_hash_before: None,
		})
	}

	pub fn path(&self) -> &Path {
		self.temporary_directory.path()
	}

	fn execute_subcommand(&self, args: &[&str]) -> &Self {
		let command = format!("`git {}` in {}", args.join(" "), self.path().display());
		log::info!("execute {}", command);
		Command::new("git")
			.current_dir(self.path())
			.args(args)
			.assert()
			.success();
		self
	}

	pub fn set_config(&self, args: &[&str]) -> &Self {
		self.execute_subcommand(&[vec!["config"], args.to_vec()].concat());
		self
	}

	fn query(&self, args: &[&str]) -> io::Result<String> {
		let query = &format!("`git {}` in {}", args.join(" "), self.path().display());
		let help_message = &format!("Failed to execute {}", query);
		let cmd = Command::new("git")
			.current_dir(self.path())
			.args(args)
			.output()
			.expect(help_message);

		log::info!("query {} exited with {}", query, cmd.status.code().unwrap());

		if cmd.status.success() {
			Ok(String::from_utf8(cmd.stdout).unwrap())
		} else {
			Err(io::Error::new(
				io::ErrorKind::Other,
				help_message.to_string(),
			))
		}
	}

	pub fn hook_path(&self, hook_name: &str) -> PathBuf {
		self.path().join(".git").join("hooks").join(hook_name)
	}

	pub fn add_hook(&self, hook_name: &str, content: &str) -> io::Result<&Self> {
		let hook_path = self.hook_path(hook_name);
		create_dir_all(hook_path.parent().unwrap())?;
		write_filecontent(&hook_path, content)?;
		#[cfg(unix)]
		make_executable(&hook_path)?;
		Ok(self)
	}
}

impl TestRepository {
	pub fn init_clean() -> io::Result<Self> {
		let mut repository = Self::new()?;

		repository.init();

		repository.last_commit_hash_before = Some(repository.get_commit_hash_for("HEAD")?);

		Ok(repository)
	}

	pub fn init_dirty() -> io::Result<Self> {
		let mut repository = Self::init_clean()?;

		repository
			.create_working_file_with(FIRST_CONTENT)?
			.make_working_file_dirty_with(SECOND_CONTENT)?;

		repository.last_commit_hash_before = Some(repository.get_commit_hash_for("HEAD")?);

		Ok(repository)
	}

	fn init(&self) -> &Self {
		self.execute_subcommand(&["init", "--template="])
			.set_config(&["user.email", "gamble@tcr.tdd"])
			.set_config(&["user.name", "Git Gamble"])
			.execute_subcommand(&["commit", "--message", "initial commit", "--allow-empty"])
	}

	fn working_file(&self) -> PathBuf {
		self.path().join("foo.txt")
	}

	fn create_working_file_with(&self, content: &str) -> io::Result<&Self> {
		let file_path_buf = self.working_file();
		let file_path = file_path_buf.as_path();
		write_filecontent(file_path, content)?;
		self.execute_subcommand(&["add", file_path.to_str().unwrap()]);
		self.execute_subcommand(&["commit", "--message", "first commit"]);
		Ok(self)
	}

	pub fn make_working_file_dirty_with(&self, content: &str) -> io::Result<&Self> {
		let file_path_buf = self.working_file();
		let file_path = file_path_buf.as_path();
		write_filecontent(file_path, content)?;
		Ok(self)
	}

	pub fn working_file_is_equal_to(&self, content: &str) -> bool {
		let file_path_buf = self.working_file();
		let file_path = file_path_buf.as_path();
		read_filecontent(file_path).unwrap() == content
	}

	pub fn changes_reverted_in_the_working_file(&self) -> bool {
		self.working_file_is_equal_to(FIRST_CONTENT)
	}

	pub fn changes_remain_in_the_working_file(&self) -> bool {
		self.working_file_is_equal_to(SECOND_CONTENT)
	}

	pub fn is_clean(&self) -> bool {
		let status = self.query(&["status", "-z"]).expect("Can't get status");

		status.is_empty()
	}

	pub fn is_dirty(&self) -> bool {
		!self.is_clean()
	}

	pub fn get_commit_hash_for(&self, revision: &str) -> io::Result<String> {
		let rev_parse = self.query(&["rev-parse", revision])?;
		let commit_hash = String::from(rev_parse.trim());

		Ok(commit_hash)
	}

	fn before_commit_is_the_same_commit_at(&self, revision: &str) -> bool {
		let commit_hash_after = &self.get_commit_hash_for(revision);

		match (&self.last_commit_hash_before, commit_hash_after) {
			(Some(last_commit_hash_before), Ok(commit_hash_after)) => {
				last_commit_hash_before == commit_hash_after
			}
			_ => false,
		}
	}

	pub fn has_no_new_commit(&self) -> bool {
		self.before_commit_is_the_same_commit_at("HEAD")
	}

	pub fn has_new_commit(&self) -> bool {
		!self.has_no_new_commit() && self.before_commit_is_the_same_commit_at("HEAD~1")
	}

	fn head_is_same_as(&self, commit_hash_other: io::Result<String>) -> bool {
		let commit_hash_head = self.get_commit_hash_for("HEAD");

		match (commit_hash_other, commit_hash_head) {
			(Ok(commit_hash_other), Ok(commit_hash_head)) => commit_hash_other == commit_hash_head,
			_ => false,
		}
	}

	pub fn head_is_different_from(&self, commit_hash_other: io::Result<String>) -> bool {
		!self.head_is_same_as(commit_hash_other)
	}

	pub fn head_is_failing_ref(&self) -> bool {
		let commit_hash_is_failing_ref = self.get_commit_hash_for("refs/gamble-is-failing");

		self.head_is_same_as(commit_hash_is_failing_ref)
	}

	pub fn head_is_not_failing_ref(&self) -> bool {
		!self.head_is_failing_ref()
	}

	pub fn head_message(&self) -> String {
		self.query(&["log", "-1", "--pretty=format:%B"])
			.expect("Can't get last commit message")
	}
}

#[cfg(unix)]
fn make_executable(path: &Path) -> Result<(), io::Error> {
	use std::os::unix::fs::PermissionsExt;

	let owner_all_permissions = PermissionsExt::from_mode(0o700);
	set_permissions(path, owner_all_permissions)?;
	Ok(())
}
