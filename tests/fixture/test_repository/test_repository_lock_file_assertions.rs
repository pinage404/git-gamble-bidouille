use speculoos::assert_that;
use speculoos::Spec;
use std::fs;
use std::io;

use crate::fixture::TestRepository;

pub trait TestRepositoryLockFileAssertions {
	fn count_lock_files(&mut self, expected_number_of_lock_files: usize) -> io::Result<()>;
}

impl<'s> TestRepositoryLockFileAssertions for Spec<'s, TestRepository> {
	fn count_lock_files(&mut self, expected_number_of_lock_files: usize) -> io::Result<()> {
		let actual_number_of_lock_files = self.subject.count_lock_files()?;

		assert_that!(&actual_number_of_lock_files).is_equal_to(expected_number_of_lock_files);
		Ok(())
	}
}

#[test]
fn count_0_when_no_lock_file_exists() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	assert_that!(&test_repository).count_lock_files(0)
}

#[cfg(unix)]
#[test]
#[should_panic]
fn panic_when_the_lock_file_folder_can_not_be_read() {
	use std::os::unix::fs::PermissionsExt;

	use assert_cmd::Command;
	use predicates::prelude::*;

	/* root ignores read permission
	 * without the following line :
	 * this test will not fail as expected for bad reason
	 */
	Command::new("whoami")
		.assert()
		.stdout(predicate::str::contains("root").not());

	let test_repository = TestRepository::init_dirty().unwrap();

	let git_dir = test_repository.path().join(".git");
	let lock_file_folder = git_dir.join("git-time-keeper.lock");
	fs::create_dir(&lock_file_folder).unwrap();

	let permission_can_not_read = PermissionsExt::from_mode(0o333);
	fs::set_permissions(&lock_file_folder, permission_can_not_read).unwrap();

	assert_that!(&test_repository).count_lock_files(0).unwrap();
}

#[test]
#[should_panic]
fn panic_with_1_when_there_are_2_lock_files() {
	let test_repository = TestRepository::init_dirty().unwrap();

	let git_dir = test_repository.path().join(".git");
	let lock_file_folder = git_dir.join("git-time-keeper.lock");
	fs::create_dir(&lock_file_folder).unwrap();
	fs::write(lock_file_folder.join("456.pid"), "").unwrap();
	fs::write(lock_file_folder.join("7890.pid"), "").unwrap();

	assert_that!(&test_repository).count_lock_files(1).unwrap();
}

#[test]
fn count_3_when_there_are_3_lock_files() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let git_dir = test_repository.path().join(".git");
	let lock_file_folder = git_dir.join("git-time-keeper.lock");
	fs::create_dir(&lock_file_folder)?;
	fs::write(lock_file_folder.join("123.pid"), "")?;
	fs::write(lock_file_folder.join("456.pid"), "")?;
	fs::write(lock_file_folder.join("7890.pid"), "")?;

	assert_that!(&test_repository).count_lock_files(3)
}
