pub mod commit_hooks;

pub mod commit_message;

pub mod dry_run;

pub mod edit_commit_message;

pub mod fixup_commit;

pub mod flags;

pub mod generate_shell_completions;

pub mod repository_path_can_be_elsewhere;

pub mod run_hook;

pub mod squash_commit;

pub mod test_command_can_be_complex;

pub mod test_command_is_required;

pub mod trigger_custom_hooks;

pub mod when_gambling_that_the_tests_fail;

pub mod when_gambling_that_the_tests_pass;
