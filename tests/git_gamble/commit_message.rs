use speculoos::assert_that;
use std::io;
use test_case::test_case;

use crate::fixture::test_repository::THIRD_CONTENT;
use crate::fixture::TestRepository;

#[test_case("-m")]
#[test_case("--message")]
#[test_log::test]
fn add_message_to_commit_when_tests_fail(flag: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let commit_message = "some message";
	let command = test_repository.gamble_with(&["--fail", flag, commit_message, "--", "false"]);

	command.success();
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_case("-m")]
#[test_case("--message")]
#[test_log::test]
fn add_message_to_commit_when_tests_pass(flag: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let commit_message = "some message";
	let command = test_repository.gamble_with(&["--pass", flag, commit_message, "--", "true"]);

	command.success();
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_case("-m")]
#[test_case("--message")]
#[test_log::test]
fn should_update_message_when_given_a_message_and_when_amending(flag: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	test_repository
		.gamble_with(&["--fail", "--", "false"])
		.success();

	test_repository.make_working_file_dirty_with(THIRD_CONTENT)?;

	let commit_message = "some message";
	test_repository
		.gamble_with(&["--fail", flag, commit_message, "--", "false"])
		.success();

	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_case("-m")]
#[test_case("--message")]
#[test_log::test]
fn should_keep_message_when_amending_without_message_provided(flag: &str) -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let commit_message = "some message";
	test_repository
		.gamble_with(&["--fail", flag, commit_message, "--", "false"])
		.success();

	test_repository.make_working_file_dirty_with(THIRD_CONTENT)?;

	test_repository
		.gamble_with(&["--fail", "--", "false"])
		.success();

	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}
