use predicates::prelude::predicate;
use speculoos::assert_that;
use std::io;

use crate::fixture::TestRepository;
use crate::fixture::THIRD_CONTENT;

#[test_log::test]
fn fixup_commit() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = test_repository.gamble_with(&["--pass", "--fixup", "HEAD", "--", "true"]);

	command.success();
	let commit_message = "fixup! first commit";
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn fixup_commit_with_commit_search() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = test_repository.gamble_with(&["--pass", "--fixup", ":/initial", "--", "true"]);

	command.success();
	let commit_message = "fixup! initial commit";
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn fixup_conflicts_with_squash() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = test_repository.gamble_with(&[
		"--pass", "--fixup", "HEAD", "--squash", "HEAD", "--", "true",
	]);

	command.failure().stderr(predicate::str::contains(
		"error: the argument '--fixup <FIXUP>' cannot be used with '--squash <SQUASH>'",
	));
	assert_that!(test_repository.has_no_new_commit());
	assert_that!(test_repository.changes_remain_in_the_working_file());
	Ok(())
}

#[test_log::test]
fn fixup_commit_overrides_reuse_message() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	test_repository
		.gamble_with(&["--fail", "--message", "some message", "--", "false"])
		.success();

	test_repository.make_working_file_dirty_with(THIRD_CONTENT)?;

	let command = test_repository.gamble_with(&["--pass", "--fixup", "HEAD", "--", "true"]);

	command.success();
	let commit_message = "fixup! some message";
	assert_that(&test_repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn fixup_commit_accepts_additional_message_parameters() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let command = test_repository.gamble_with(&[
		"--pass",
		"--fixup",
		"HEAD",
		"--message",
		"More information",
		"--",
		"true",
	]);

	command.success();
	let commit_message = "fixup! first commit\n\nMore information\n";
	assert_that(&test_repository.head_message().as_str()).is_equal_to(commit_message);
	Ok(())
}
