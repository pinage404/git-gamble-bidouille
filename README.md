<!-- markdownlint-disable-next-line MD033 MD041 -->
<img src="https://git-gamble.is-cool.dev/assets/logo/git-gamble.svg" width="100" title="git-gamble's logo" alt="git-gamble's logo" />

# git-gamble

[![Crate available on Crates.io](https://img.shields.io/crates/d/git-gamble.svg?logo=rust)](https://crates.io/crates/git-gamble)
[![AppImage available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=AppImage&prefix=v&logo=linux)](https://gitlab.com/pinage404/git-gamble/-/packages)
[![Debian available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Debian&prefix=v&logo=debian)](https://gitlab.com/pinage404/git-gamble/-/packages)
[![Chocolatey available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Chocolatey&prefix=v&logo=chocolatey)](https://gitlab.com/pinage404/git-gamble/-/packages)
[![Homebrew available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Homebrew&prefix=v&logo=homebrew)](https://gitlab.com/pinage404/git-gamble/-/packages)

[![dependency status](https://deps.rs/crate/git-gamble/2.9.0/status.svg)](https://deps.rs/crate/git-gamble/2.9.0)
[![coverage report](https://gitlab.com/pinage404/git-gamble/badges/main/coverage.svg)](https://gitlab.com/pinage404/git-gamble/-/commits/main)
[![CodeScene Code Health](https://codescene.io/projects/47370/status-badges/code-health)](https://codescene.io/projects/47370)
[![pipeline status](https://img.shields.io/gitlab/pipeline/pinage404/git-gamble?label=pipeline&logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/pipelines)
[![AppVeyor for Homebrew status](https://ci.appveyor.com/api/projects/status/qrd9p11ec2kbt1xs?svg=true)](https://ci.appveyor.com/project/pinage404/git-gamble)
[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

[![License ISC](https://img.shields.io/crates/l/git-gamble.svg)](https://gitlab.com/pinage404/git-gamble/blob/main/LICENSE)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)
[![GitLab stars](https://img.shields.io/gitlab/stars/pinage404/git-gamble?style=social)](https://gitlab.com/pinage404/git-gamble)

`git-gamble` is a tool that
blends [TDD (Test Driven Development)](https://en.wikipedia.org/wiki/Test-driven_development) + [TCR (`test && commit || revert`)](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864)
to make sure to **develop** the **right** thing 😌,
**baby step by baby step** 👶🦶

[Original idea](https://github.com/FaustXVI/tcrdd) by Xavier Detant

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD007 MD010 -->
- [How to install ?](#how-to-install-)
- [How to use ?](#how-to-use-)
  - [Demo](#demo)
- [What is it ?](#what-is-it-)
- [What is new ?](#what-is-new-)
- [Do you like this project ?](#do-you-like-this-project-)
<!-- markdownlint-restore -->

[Get started](https://git-gamble.is-cool.dev)

## How to install ?

Read [the installation manual](https://git-gamble.is-cool.dev/install)

## How to use ?

Read [the usage manual](https://git-gamble.is-cool.dev/usage)

### Demo

For more detailed example, watch the [demo](https://git-gamble.is-cool.dev/usage/demo/index.html) or the watch [the slides about the demo](https://git-gamble.is-cool.dev/slides_demo)

[![asciicast](https://asciinema.org/a/496959.svg)](https://git-gamble.is-cool.dev/usage/demo/index.html)

## What is it ?

Watch [the slides about the theory](https://git-gamble.is-cool.dev/slides_theory)

## What is new ?

Read [the changelog](https://gitlab.com/pinage404/git-gamble/-/blob/main/CHANGELOG.md)

## Do you like this project ?

<!-- markdownlint-disable-next-line MD039 MD045 -->
- If yes, please [add a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/git-gamble?style=social)](https://gitlab.com/pinage404/git-gamble)
<!-- markdownlint-disable-next-line MD045 -->
- If no, please [open an issue](https://gitlab.com/pinage404/git-gamble/-/issues) to give your feedbacks [![open an issue](https://img.shields.io/gitlab/issues/all/pinage404%2Fgit-gamble?logo=gitlab)](https://gitlab.com/pinage404/git-gamble/-/issues)
