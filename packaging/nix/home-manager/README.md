# Home-Manager / user level

Install [Home Manager](https://nix-community.github.io/home-manager/)

Copy [`flake.nix`](./flake.nix)
to `~/.config/nixpkgs/flake.nix`

Copy [`home.nix`](./home.nix)
to `~/.config/nixpkgs/home.nix`

Then run this command

```shell
home-manager switch --flake ".#jdoe"
```
