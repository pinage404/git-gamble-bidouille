# Installation

There is several methods to install depending on your operating system:

- [Nix / NixOS](./methods/nix.md)
- [AppImage](./methods/AppImage.md)
- [Debian](./methods/debian.md)
- [Mac OS X / Homebrew](./methods/homebrew.md)
- [Windows / Chocolatey](./methods/chocolatey.md)
- [Cargo](./methods/chocolatey.md)
- [Download the binary](./methods/binary.md)

{{#include ./_please_improve_install.md}}
