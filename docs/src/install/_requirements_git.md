<!-- markdownlint-disable-next-line MD041 -->
`git-gamble` doesn't repackage `git`, it uses the one installed on your system

You have to [install `git` manually](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Make sure it is available in [your `$PATH`](https://superuser.com/a/284361), you can check it with this command

```sh
git --help
```

If it has been **well settled**, it should output something like this :

```txt
git version 2.45.2
```

Else if it has been **badly settled**, it should output something like this :

```txt
git: command not found
```
