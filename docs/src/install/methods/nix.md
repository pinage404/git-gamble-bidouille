# Install using Nix

## Requirements

1. [Install Nix](https://github.com/DeterminateSystems/nix-installer)
1. Check the installation with this command

   ```sh
   nix --version
   ```

   If it has been **well settled**, it should output something like this :

   ```txt
   nix (Nix) 2.22.3
   ```

   Else if it has been **badly settled**, it should output something like this :

   ```txt
   nix: command not found
   ```

## Install

Use without installing

```sh
export NIX_CONFIG="extra-experimental-features = flakes nix-command"
nix run gitlab:pinage404/git-gamble -- --help
```

To install it at

- project level with [`flake.nix`, see example](https://gitlab.com/pinage404/git-gamble/-/tree/main/packaging/nix/flake/)

  **TL;DR**

  ```sh
  export NIX_CONFIG="extra-experimental-features = flakes nix-command"
  nix flake init --template gitlab:pinage404/git-gamble
  ```

- project level with [`shell.nix`, see example](https://gitlab.com/pinage404/git-gamble/-/tree/main/packaging/nix/legacy/)
- user level with [Home Manager, see example](https://gitlab.com/pinage404/git-gamble/-/tree/main/packaging/nix/home-manager/)

## DirEnv

To automate the setup of the environment it's recommanded to install [DirEnv](https://direnv.net/)

Add in `.envrc`

```sh
use nix
```

Then run this command

```sh
direnv allow
```

## Cachix

If you want to avoid rebuilding from scratch, you can use [Cachix](https://app.cachix.org/cache/git-gamble#pull)

```sh
cachix use git-gamble
```

{{#include ../_check_the_installation.md}}

## Update

```sh
export NIX_CONFIG="extra-experimental-features = flakes nix-command"
nix flake update
```
