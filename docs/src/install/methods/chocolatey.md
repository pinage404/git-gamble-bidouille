# Install using Chocolatey on Windows

[![Chocolatey available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Chocolatey&prefix=v&logo=chocolatey)](https://gitlab.com/pinage404/git-gamble/-/packages)

## Requirements

### Git

{{#include ../_requirements_git.md}}

### Chocolatey

1. [Install Chocolatey](https://community.chocolatey.org/courses)
1. Check the installation with this command

   ```sh
   choco --version
   ```

   If it has been **well settled**, it should output something like this :

   ```txt
   2.3.0
   ```

   Else if it has been **badly settled**, it should output something like this :

   ```txt
   choco: command not found
   ```

## Install

1. Go to [the package registry page](https://gitlab.com/pinage404/git-gamble/-/packages)
1. Go to the latest version of `git-gamble.portable`
1. Download the latest version `git-gamble.portable.2.9.0.nupkg`
1. Install package, this may need an administrative shell

   Follow [GitLab's documentation](https://docs.gitlab.com/ee/user/packages/nuget_repository/index.html) and [Chocolatey's documentation](https://docs.chocolatey.org/en-us/choco/commands/install)

   ```sh
   choco install ./git-gamble.portable.2.9.0.nupkg
   ```

Note: [the latest version, not yet released, is also available](https://gitlab.com/pinage404/git-gamble/-/jobs/artifacts/main/raw/packaging%20Chocolatey20git-gamble.portable.2.9.0.nupkg?job=package%20Chocolatey)

{{#include ../_check_the_installation.md}}

## Update

```sh
choco upgrade git-gamble
```

## Uninstall

```sh
choco uninstall git-gamble
```

{{#include ../_please_improve_install.md}}
