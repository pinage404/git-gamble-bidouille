# Install using AppImage

[![AppImage available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=AppImage&prefix=v&logo=linux)](https://gitlab.com/pinage404/git-gamble/-/packages)

{{#include ../_requirements.md}}

## Install

1. Download the [latest released version](https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-AppImage/2.9.0/git-gamble-v2.9.0-x86_64.AppImage)

   ```sh
   curl --location "https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-AppImage/2.9.0/git-gamble-v2.9.0-x86_64.AppImage" --output git-gamble-v2.9.0-x86_64.AppImage
   ```

1. Make it executable

   ```sh
   chmod +x git-gamble-v2.9.0-x86_64.AppImage
   ```

1. [Put it your `$PATH`](https://superuser.com/a/284361)

   ```sh
   # for example
   mkdir -p ~/.local/bin
   ln git-gamble-v2.9.0-x86_64.AppImage ~/.local/bin/git-gamble
   export PATH+=":~/.local/bin"
   ```

Note: [the latest version, not yet released, is also available](https://gitlab.com/pinage404/git-gamble/-/jobs/artifacts/main/raw/target%2Fgit-gamble-v-x86_64.AppImage?job=package%20AppImage)

{{#include ../_check_the_installation.md}}

## Update

There is no update mechanism

## Uninstall

Just remove the binary from [your `$PATH`](https://superuser.com/a/284361)
