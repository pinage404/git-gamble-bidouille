# Install using Cargo

[![Crate available on Crates.io](https://img.shields.io/crates/d/git-gamble.svg?logo=rust)](https://crates.io/crates/git-gamble)

## Requirements

### Git

{{#include ../_requirements_git.md}}

### Cargo

1. [Install Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)
1. Check the installation with this command

   ```sh
   choco --version
   ```

   If it has been **well settled**, it should output something like this :

   ```txt
   cargo 1.80.0 (376290515 2024-07-16)
   ```

   Else if it has been **badly settled**, it should output something like this :

   ```txt
   cargo: command not found
   ```

## Install

Run the following command

```sh
cargo install git-gamble
```

[Add `~/.cargo/bin` to your `$PATH`](https://superuser.com/a/284361)

Fish:

```fish
set --export --append PATH ~/.cargo/bin
```

Bash / ZSH:

```bash
export PATH=$PATH:~/.cargo/bin
```

{{#include ../_check_the_installation.md}}

## Update

```sh
cargo update --package git-gamble
```

## Uninstall

```sh
cargo uninstall git-gamble
```
