# VSCode

If you want to run `git-gamble` from VSCode

## Setup

Add this in your [`.vscode/tasks.json`](https://code.visualstudio.com/docs/editor/tasks#_custom-tasks)

```json
{{#include ../../../.vscode/tasks.json}}
```

Remove `direnv exec .` if you don't use [DirEnv](https://direnv.net/) yet

## Execute

### Using command palette

<!-- markdownlint-disable-next-line MD033 -->
1. Open the Command Palette (<kbd>F1</kbd> or <kbd>CTRL + Shift + P</kbd>)
1. Select `Tasks: Run Task`
1. Select the task (e.g. `Gamble refactor`)

### Using shortcuts

You can [bind keyboard shortcuts](https://code.visualstudio.com/docs/editor/tasks#_binding-keyboard-shortcuts-to-tasks)

### Using buttons in the sidebar

You can install [the Task Explorer extension](https://marketplace.visualstudio.com/items?itemName=spmeesseman.vscode-taskexplorer) if you want to trigger using click
