---
favicon: '/logo.svg'
info: >
  git-gamble is a tool that
  blends TDD + TCR to make sure to develop the right thing 😌,
  baby step by baby step 👶🦶
addons:
  - slidev-component-zoom
selectable: true
canvasWidth: 900
lineNumbers: true

hideInToc: true
---

# TCRDD theory

---
layout: center
---

<nav><Toc /></nav>

---
layout: section
---

# TDD

---
layout: cover
src: ./docs/theory/tdd/_description.md
---

---
layout: center
src: ./docs/theory/tdd/graph/_all_in_one.md
---

---
layout: center
src: ./docs/theory/tdd/graph/_red.md
zoom: 0.95
---

---
layout: center
src: ./docs/theory/tdd/graph/_green.md
zoom: 0.9
---

---
layout: center
src: ./docs/theory/tdd/graph/_refactor.md
zoom: 1.75
---

---
layout: section
---

# TCR

---
layout: cover
src: ./docs/theory/tcr/_description.md
---

---
layout: center
src: ./docs/theory/tcr/graph/_simplified.md
zoom: 2.2
---

---
layout: center
src: ./docs/theory/tcr/graph/_overview_tdd.md
zoom: 2.2
---

---
layout: cover
src: ./docs/theory/tcr/_but.md
---

---
layout: center
src: ./docs/theory/tcr/graph/_detailed_view.md
zoom: 2.2
---

---
layout: section
---

# TCRDD

---
layout: cover
src: ./docs/theory/tcrdd/_description.md
---

---
layout: center
src: ./docs/theory/tcrdd/graph/_all_in_one.md
zoom: 0.85
---

---
layout: center
src: ./docs/theory/tcrdd/graph/_red.md
zoom: 0.95
---

---
layout: center
src: ./docs/theory/tcrdd/graph/_green.md
zoom: 1.05
---

---
layout: center
src: ./docs/theory/tcrdd/graph/_refactor.md
zoom: 1.6
---

---
layout: fact
---

[`git-gamble` <span class="logo mini"></span>](https://git-gamble.is-cool.dev)
is a tool that helps to use the TCRDD method

---
src: ./pages/link_to_slides_why.md
---

---
src: ./pages/link_to_slides_demo.md
---

---
src: ./pages/questions.html
---
