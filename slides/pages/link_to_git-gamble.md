---
layout: fact
---

[`git-gamble` <span class="logo mini"></span>](https://git-gamble.is-cool.dev)
is a tool that helps to develop the right thing 😌,
baby step by baby step 👶🦶
